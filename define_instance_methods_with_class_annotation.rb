# Defining Instance Methods with Class Annotation, page 178

class Thing
  def self.states(*args)
    attr_accessor :state

    args.each do |arg|
      define_method arg do
        self.state = arg
      end
    end
  end

  states :failure, :error, :success
end

thing = Thing.new
thing.success
thing.state # => :success
