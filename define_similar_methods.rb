# Using def_each to Define Similar Methods , page 176

class Class
  def def_each(*method_names, &block)
    method_names.each do |method_name|
      define_method method_name do
        instance_exec method_name, &block
      end
    end
  end
end

class Thing
  def_each :failure, :error, :success do |method_name|
    self.state = method_name
  end

  attr_accessor :state
end

thing = Thing.new
thing.success
thing.state # => :success
