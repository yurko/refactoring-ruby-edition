# Defining Methods By Extending a Dynamically defined Module, 179

class Hash
  def to_module
    hash = self
    Module.new do
      hash.each_pair do |key, value|
        define_method key do
          value
        end
      end
    end
  end
end

class Thing
  def initialize(params)
    self.extend(params.to_module)
  end
end

Thing.new(foo: :bar).foo # => :bar
