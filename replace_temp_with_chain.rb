# Replace Temp with Chain, page 137

class Select
  def self.with_option(option)
    new.tap { |select| select.options << option }
  end

  def options
    @options ||= []
  end

  def and(option)
    options << option
    self
  end
end

select = Select.with_option(1).and(2).and(3)
select.options # => [1, 2, 3]
