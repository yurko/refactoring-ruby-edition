# Introduce Class Annotation, page 162

module CustomInitializers
  def hash_initializer(*attr_names)
    define_method(:initialize) do |*args|
      data = args.first || {}
      attr_names.each do |attr_name|
        instance_variable_set "@#{attr_name}", data[attr_name]
      end
    end
  end
end

Class.send :include, CustomInitializers

class SearchCriteria
  hash_initializer :author_id, :publisher_id, :isbn
end

SearchCriteria.new(author_id: 1, publisher_id: 2, isbn: 3)
